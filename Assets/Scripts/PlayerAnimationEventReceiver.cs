﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationEventReceiver : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ReleaseMissle() {
		GameManager.instance.player.GetComponent<PlayerController>().ReleaseMissle();
	}
}
