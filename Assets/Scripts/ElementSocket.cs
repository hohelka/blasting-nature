using System.Collections;
using System.Collections.Generic;
[System.Serializable]
public class ElementSocket {

    public ElementSocket() {

    }

    public ElementSocket(ElementsEnum elementType, bool active, LampEnum lamp) {
        this.elementType = elementType;
        this.active = active;
        this.lamp = lamp;
    }
    public ElementsEnum elementType {get; set;}

    public bool active {get; set;} 

    public LampEnum lamp {get; set;}
}