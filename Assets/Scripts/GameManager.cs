﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance = null; 
	public int level = 1;
	// Use this for initialization
	public int alwaysAvailableSegments = 5;

	public float timeToSpawnElement = 5f;
	public float score = 0;
	private bool firstElementSpawn = false;
	private bool startText = false;
	public GameObject element;

	public GameObject player;

	private GameObject countdown;

	private bool menu = false;
	private bool dead = false;

	private GameObject gameEmpty;
	private GameObject menuEmpty;

	private GameObject deadPanel;
	public Vector3 lastElementPosition = new Vector3(-10f, -10f, -10f);


	void Awake() {
            if (instance == null) {
                instance = this;
			} else if (instance != this) {
            	Destroy(gameObject);   
				return; 
			}
            //Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);
			SceneManager.sceneLoaded += OnSceneLoaded;
    }
	void Start () {
		
	}

	void StopGame() {
		Time.timeScale = 0.0f;
		menu = false;
		gameEmpty.SetActive(false);
		menuEmpty.SetActive(true);
		
	}

	public void ExitGame() {
		Application.Quit();
	}

	public void StartGame() {
		Time.timeScale = 1.0f;
		gameEmpty.SetActive(true);
		menuEmpty.SetActive(false);
		menu = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (menu) {
			if (Input.GetKeyDown(KeyCode.Escape)) {
				StopGame();
				return;
			}
			GameObject[] segments = GameObject.FindGameObjectsWithTag("Segment");
			List<GameObject> segmentsList = new List<GameObject>(segments);
			FindAndDestroySegmentsPassed(segmentsList);
			CreateNewSegmentsAhead(segmentsList);
			SpawnElement();
			updateCountdownTextAnimation();
			updateScore(5f * Time.deltaTime);
			increaseLevel();
		}
		if (dead) {
			if (Input.GetKeyDown(KeyCode.R)) {
				StartAgain();
			} 
		}
	}

	void increaseLevel() {
		if (score >= 50 && score < 100) {
			level = 2;
		} else if (score >= 100 && score < 150) {
			level = 3;
		} else if (score >= 150 && score < 200) {
			level = 4;
		} else if (score >= 200 && score < 250) {
			level = 5;
		} else if (score >= 250) {
			level = 6;
		}
	}

	void updateScore(float numberToUpdate) {
		score += numberToUpdate;
		GameObject.Find("Score").GetComponent<UnityEngine.UI.Text>().text = "Score: " + score.ToString("F0");
	}

	public void updateScoreAfterKill(float socketNumber) {
		updateScore(socketNumber * 5f);
	}

	void updateCountdownTextAnimation() {
		if (!firstElementSpawn) {
			if (timeToSpawnElement <= 3.0f) {
				countdown.SetActive(true);
				countdown.GetComponent<UnityEngine.UI.Text>().text = "3";
				countdown.GetComponent<Animation>().Play();
			}
			if (timeToSpawnElement <= 2.0f) {
				countdown.GetComponent<UnityEngine.UI.Text>().text = "2";
				countdown.GetComponent<Animation>().Play();
			}
			if (timeToSpawnElement <= 1.0f) {
				countdown.GetComponent<UnityEngine.UI.Text>().text = "1";
				countdown.GetComponent<Animation>().Play();
			}
		}
		//if (firstElementSpawn && !startText) {
		//		countdown.GetComponent<UnityEngine.UI.Text>().text = "Let's go";
		//		countdown.GetComponent<Animation>().Play();
		//		startText = true;
		//}
	}

	

	void FindAndDestroySegmentsPassed(List<GameObject> segments) {
		foreach (GameObject segment in segments) {
			if (segment.transform.position.z - player.transform.position.z <= -10f) {
				Destroy(segment);
			}
		}
	}

	void CreateNewSegmentsAhead(List<GameObject> segments) {
		if (segments.Count < alwaysAvailableSegments) {
			GameObject lastSegment = segments.LastOrDefault();
			Vector3 lastSegmentPosition = lastSegment.transform.position;
			lastSegmentPosition.z += 10f;
			Instantiate(lastSegment, lastSegmentPosition, lastSegment.transform.rotation);
		}
	}

	void SpawnElement() {
		timeToSpawnElement -= Time.deltaTime;
		Debug.Log(GameObject.FindWithTag("Element"));
		if (timeToSpawnElement <= 0f && GameObject.FindGameObjectsWithTag("Element").Count() <= 10) {
			Vector3 newElementPosition;
			if (lastElementPosition.Equals(new Vector3(-10f, -10f, -10f))) {
			 	newElementPosition = player.transform.position;
			} else {
				newElementPosition = lastElementPosition;
			}
			newElementPosition.z += 10f;
			GameObject newElement = Instantiate(element, newElementPosition, player.transform.rotation);
			lastElementPosition = newElement.transform.position;
			if (!firstElementSpawn) {
				firstElementSpawn = true;
			}
			timeToSpawnElement = 1f;
			
		}
	}

	void EndGame() {
		Time.timeScale = 0f;
		dead = true;
		deadPanel.SetActive(true);
	}

	void StartAgain() {
		Time.timeScale = 1f;
		
		deadPanel.SetActive(false);
		
		SceneManager.LoadScene(0, LoadSceneMode.Single);
	}

	void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode) {
		gameEmpty = GameObject.Find("Game");
			menuEmpty = GameObject.Find("Menu");
			player = GameObject.FindWithTag("Player");
			countdown = GameObject.Find("Countdown");
			countdown.SetActive(false);
			deadPanel = GameObject.Find("DeadPanel");
			deadPanel.SetActive(false);
			timeToSpawnElement = 5f;
			firstElementSpawn = false;
			score = 0;
			level = 1;
			if (!dead) {
				StopGame();
			} else {
				StartGame();
				dead = false;
			}
	}

	public void Dead() {
		EndGame();
		
	}
}
