﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface MissleInterface {
	void Shoot();

	void InteractWithElement(ElementInterface element);

	void PrepareMissle();


}
