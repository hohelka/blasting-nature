﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerController : MonoBehaviour {
	
	CharacterController characterController;

	Animator animator;

	public GameObject fireMissle;
	public GameObject iceMissle;
	public GameObject airMissle;
	public GameObject earthMissle;

	Transform misslePlace;

	public float speed = 3f;
	// Use this for initialization
	void Start () {
		animator = GetComponentInChildren<Animator>();
		misslePlace = transform.Find("MisslePlace");
		characterController = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Vector3 moveDir = transform.forward;
		moveDir = transform.TransformDirection(moveDir);
		characterController.Move(moveDir * speed * Time.fixedDeltaTime);
		
	}

	void Update() {
		CheckIfAnyMissleFired();
	}

	void CheckIfAnyMissleFired() {
			if (Input.GetKeyDown(KeyCode.Q) && 
				transform.GetComponentsInChildren<MissleElement>().Where(me => {return me.elementType.Equals(ElementsEnum.Fire);}).Count() <= 6) {
				Instantiate(fireMissle, misslePlace.transform.position, misslePlace.transform.rotation).transform.SetParent(transform);
				animator.SetTrigger("Shoot");
			}
			if (Input.GetKeyDown(KeyCode.W) && 
				transform.GetComponentsInChildren<MissleElement>().Where(me => {return me.elementType.Equals(ElementsEnum.Water);}).Count() <= 6) {
				Instantiate(iceMissle, misslePlace.transform.position, misslePlace.transform.rotation).transform.SetParent(transform);
				animator.SetTrigger("Shoot");
			}
			if (Input.GetKeyDown(KeyCode.E) && 
				transform.GetComponentsInChildren<MissleElement>().Where(me => {return me.elementType.Equals(ElementsEnum.Air);}).Count() <= 6) {
				Instantiate(airMissle, misslePlace.transform.position, misslePlace.transform.rotation).transform.SetParent(transform);
				animator.SetTrigger("Shoot");
			}
			if (Input.GetKeyDown(KeyCode.R) && 
				transform.GetComponentsInChildren<MissleElement>().Where(me => {return me.elementType.Equals(ElementsEnum.Nature);}).Count() <= 6) {
				Instantiate(earthMissle, misslePlace.transform.position, misslePlace.transform.rotation).transform.SetParent(transform);
				animator.SetTrigger("Shoot");
			}
	}

	public void ReleaseMissle() {
		GetComponent<AudioSource>().Play();
		foreach (MissleInterface item in GetComponentsInChildren<MissleInterface>())
		{
			item.Shoot();
		} 
		/*if (fireMisslefireMissle.gameObject.activeSelf) {
			GetComponent<AudioSource>().Play();
			fireMissle.GetComponent<MissleInterface>().Shoot();
		}
		if (iceMissle.gameObject.activeSelf) {
			GetComponent<AudioSource>().Play();
			iceMissle.GetComponent<MissleInterface>().Shoot();
		}
		if (airMissle.gameObject.activeSelf) {
			GetComponent<AudioSource>().Play();
			airMissle.GetComponent<MissleInterface>().Shoot();
		}
		if (earthMissle.gameObject.activeSelf) {
			GetComponent<AudioSource>().Play();
			earthMissle.GetComponent<MissleInterface>().Shoot();
		}*/
	}

	private void OnControllerColliderHit(ControllerColliderHit hit) {
		Debug.Log("HIT!");
		if (hit.collider.CompareTag("Element")) {
			GameManager.instance.Dead();
		}
	}

}
