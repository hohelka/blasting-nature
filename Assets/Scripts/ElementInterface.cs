﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ElementInterface {
	void Killed();

	void CheckIfAnySocketFilled(ElementsEnum hittingElement);
	void CheckIfKilled();

	void PrepareElement();

	void Hit(ElementsEnum element);


}
