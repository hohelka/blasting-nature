﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		gameObject.transform.Find("Start").GetComponent<Button>().onClick.AddListener(GameManager.instance.StartGame);
		gameObject.transform.Find("Exit").GetComponent<Button>().onClick.AddListener(GameManager.instance.ExitGame);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
