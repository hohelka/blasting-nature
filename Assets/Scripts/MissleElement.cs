﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissleElement : MonoBehaviour, MissleInterface {
    public ElementsEnum elementType;

    public float elementSpeed = 5f;

    private bool shot = false;
    private float timeAfterShot = 0f;
    private Vector3 positionOffset = new Vector3(0, 0, 0.96f);
    private Quaternion startRotationOffset;



    // Use this for initialization


    void Start () {
		PrepareMissle();
	}
	
	// Update is called once per frame
	void Update () {
		if (shot) {
            transform.Translate(transform.forward * elementSpeed * Time.deltaTime);
            timeAfterShot += Time.deltaTime;
            CheckIfMissleReturn();
        }
	}

	public void PrepareMissle() {
    //    elementType = ElementsEnum.Fire;
	}

    public void Shoot()
    {
        shot = true;
    }

    void CheckIfMissleReturn() {
        if (timeAfterShot >= 3f) {
            StopShot();
        }
    }

    public void InteractWithElement(ElementInterface element)
    {
        element.Hit(elementType);
        StopShot();
    }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Element")) {
            InteractWithElement(other.transform.parent.parent.GetComponent<ElementInterface>());
        }
    }

    private void StopShot() {
        Destroy(gameObject);
    }
}
