﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public enum ElementsEnum {
		Fire,
		Water,
		Nature,
		Air,
}

[System.Serializable]
public enum LampEnum {
		lamp0,
		lamp1,
		lamp2,
		lamp3,
		lamp4,
		lamp5,
}
[System.Serializable]
public class Enums : MonoBehaviour {

	public ElementsEnum elements;
}
