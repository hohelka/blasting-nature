﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Element : MonoBehaviour, ElementInterface {
    
    public List<ElementSocket> sockets = new List<ElementSocket>();

    private List<LampEnum> lampList;

    
	public Material lampMaterial;
    


    // Use this for initialization


    void Start () {
        //AddNewMaterialToAllLamps();
        PrepareLampStartingList();
		PrepareElement();
        RemoveUncessesaryLampPrefabs();
        ReloadLamps();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    
    //private void AddNewMaterialToAllLamps() {
    //    foreach (GameObject lamp in GameObject.FindGameObjectsWithTag("Lamp")) {
    //        lamp.GetComponentInChildren<Renderer>().material = lampMaterial;
    //    }
    //}

    private void RemoveUncessesaryLampPrefabs() {
        foreach (LampEnum lampEnum in lampList) {
            Destroy(transform.Find(lampEnum.ToString()).gameObject);
        }
    }

    private void ReloadLamps() {
        foreach (ElementSocket socket in sockets) {
            GameObject lamp = transform.Find(socket.lamp.ToString()).gameObject;
            lamp.GetComponentInChildren<Renderer>().material.SetTexture("_MainTex", Resources.Load("LampTextures/" + socket.elementType.ToString() + (socket.active ? "active" : "nonactive") + "/lamp_None_AlbedoTransparency") as Texture);
        }
    }

	public void PrepareElement() {
        for (int i = 0; i < Random.Range(1, GameManager.instance.level); i++) {
            sockets.Add(AddSocket(RandomSelectElementType() , false));
        }
	}

    public ElementSocket AddSocket(ElementsEnum elementsEnum, bool active) {
        return new ElementSocket(elementsEnum, active, getRandomLampFromList());
    }

    public LampEnum getRandomLampFromList() {
        LampEnum randomLampEnum = lampList.ElementAt(Random.Range(0, lampList.Count));
        lampList.Remove(randomLampEnum);
        return randomLampEnum;
    }

    private void PrepareLampStartingList() {
        lampList = new List<LampEnum>(new LampEnum[] {LampEnum.lamp0, LampEnum.lamp1, LampEnum.lamp2, LampEnum.lamp3, LampEnum.lamp4, LampEnum.lamp5});
    }
    
    private ElementsEnum RandomSelectElementType() {
        switch(Random.Range((int) 0, (int) 4)) {
            case 0:
            Debug.Log("ADDING AIR");
                return ElementsEnum.Air;
            case 1:
            Debug.Log("ADDING FIRE");
                return ElementsEnum.Fire;
            case 2:
            Debug.Log("ADDING NATURE");
                return ElementsEnum.Nature;
            case 3:
            Debug.Log("ADDING WATER");
                return ElementsEnum.Water;
            default:
                return ElementsEnum.Air;
        }
    }

	
    public void CheckIfKilled()
    {
        if (sockets.FindAll(socket => !socket.active).Count == 0) {
            Killed();
        }
    }

    public void Killed()
    {
        GameManager.instance.updateScoreAfterKill(sockets.Count * 5f);
        Destroy(gameObject);
    }

    public void Hit(ElementsEnum element)
    {
        CheckIfAnySocketFilled(element);
        ReloadLamps();
    }

    public void CheckIfAnySocketFilled(ElementsEnum hittingElement)
    {
        
        ElementSocket elementSocket = sockets.Find(socket => socket.elementType.Equals(hittingElement) && !socket.active);
        if (elementSocket != null) {
            elementSocket.active = true;
        } else {
            ResetSockets();
        }
        CheckIfKilled();
    }

    public void ResetSockets() {
        sockets.ForEach(socket => socket.active = false);
    }
}
