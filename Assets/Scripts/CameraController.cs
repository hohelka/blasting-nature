﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

	Vector3 offset;
	public Transform objectToFollow;
	// Use this for initialization
	void Start () {
		offset = transform.position - objectToFollow.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = objectToFollow.position + offset;
	}
}
